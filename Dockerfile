# syntax=docker/dockerfile:1
FROM python:3.10.8-slim-buster
WORKDIR /python-docker
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
EXPOSE 80
CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0"]